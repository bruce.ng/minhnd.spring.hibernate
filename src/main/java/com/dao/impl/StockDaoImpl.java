package com.dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.dao.StockDao;
import com.model.Stock;

@Repository("stockDao")
public class StockDaoImpl extends HibernateDaoSupport implements StockDao{
	
	@Override
	public void save(Stock stock){
		getHibernateTemplate().save(stock);
	}
	
	@Override
	public void update(Stock stock){
		getHibernateTemplate().update(stock);
	}
	
	@Override
	public void delete(Stock stock){
		getHibernateTemplate().delete(stock);
	}
	
	@Override
	public Stock findByStockCode(String stockCode){
		List<Stock> list = (List<Stock>)getHibernateTemplate().find("from Stock where stockCode=?",stockCode);
		return (Stock)list.get(0);
	}

}
